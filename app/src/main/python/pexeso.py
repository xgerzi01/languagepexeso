import random


class Player:
    id = 1

    def __init__(self, name="dflt"):
        self.__score = 0
        if name == "dflt":
            self.__name = "player" + str(Player.id)
            Player.id += 1
        else:
            self.__name = name

    def inc_score(self):
        self.__score += 1

    def get_score(self):
        return self.__score

    def reset_score(self):
        self.__score = 0

    def __str__(self):
        return self.__name + "\tscore: " + str(self.__score)


class Pexeso:
    def __init__(self, width=4, height=4, *players):
        self.finished = False
        self.__cntTakenCards = 0
        self.__cntMoves = 0
        self.__cntPlayers = 0
        self.__width = 0
        self.__height = 0
        self.gameArr = list()
        self.players = list()
        self.set_board_size(width, height)
        self.__actPlayerIndex = 0;
        for player in players:
            self.players.append(player)
            self.__cntPlayers += 1

    def add_player(self, player):
        self.players.append(player)
        self.__cntPlayers += 1

    def reset(self):
        self.gameArr = list(range(self.__width * self.__height))
        random.shuffle(self.gameArr)
        self.finished = False
        for player in self.players:
            player.reset_score()

    def get_card(self, x, y):
        if x >= self.__width or y >= self.__height:
            # raise Exception("OUT OF RANGE")
            return -1
        return int(self.gameArr[y * self.__width + x])

    # card with -1 is invalid
    def __delete_card(self, x, y):
        self.gameArr[y * self.__width + x] = -1

    def get_actual_player(self):
        return self.players[self.__actPlayerIndex]

    def get_act_player_index(self):
        return self.__actPlayerIndex

    def play_move(self, x1, y1, x2, y2):
        card1 = self.get_card(x1, y1)
        card2 = self.get_card(x2, y2)
        if card1 != -1 and card2 != -1 and not (x1 == x2 and y1 == y2):
            if card1 >> 1 == card2 >> 1:
                self.__delete_card(x1, y1)
                self.__delete_card(x2, y2)
                self.get_actual_player().inc_score()
                self.__cntMoves += 1
                self.__cntTakenCards += 2
                if self.__cntTakenCards >= self.__height * self.__width:
                    self.finished = True
                return True
            else:
                self.__actPlayerIndex = (self.__actPlayerIndex + 1) % self.__cntPlayers
                self.__cntMoves += 1
                return False
        else:
            pass

    def set_board_size(self, width, height):
        if (width * height % 2) == 1:
            raise Exception("Number of cards cannot be odd")
        self.__width = width
        self.__height = height
        self.reset()

    def get_score(self):
        retList = []
        for player in self.players:
            retList.append(player.get_score())
        return retList

    def is_finished(self):
        return self.finished;

    def __str__(self):
        text = ""
        for y in range(self.__height):
            text += " | "
            for x in range(self.__width):
                text += str(pexeso.get_card(x, y)) + " | "
            text += "\n"
        return text


"""
# TESTS
player1 = Player("Miláček")
player2 = Player("Já")
pexeso = Pexeso(4, 4, player1, player2)

while not pexeso.finished:
    print(pexeso)
    print("Hráč:\n", pexeso.get_actual_player())
    print()
    print("pick card 1: ")
    x1 = int(input())
    y1 = int(input())
    print("pick card 2: ")
    x2 = int(input())
    y2 = int(input())
    if pexeso.play_move(x1, y1, x2, y2):
        print("Found")
    print("\n")
    """
