import random

def return_hard_data_decks():
    retList = [[0, "Jidlo - Food", "Devs", "CZ", "EN", "Zakladní balíček se slovíčky ohledně jídla", 20],
        [-1, "Zvířata - Animals", "Devs", "CZ", "EN", "Zakladní balíček se zvířaty", 20],
        [-2, "Sport", "Devs", "CZ", "EN", "Zakladní balíček se slovíčky ohledně sportu", 20]]
    return retList

def return_hard_data_cards(idBalicku):
    if idBalicku == -1:
        return [["pes", "dog"], ["kočka", "cat"], ["koza", "goat"], ["kráva", "cow"], ["ovce", "sheep"], ["ryba", "fish"], ["slon", "elephant"],
                ["papoušek", "parrot"], ["kuře", "chicken"], ["kůň", "horse"], ["krab", "crab"], ["želva", "turtle"], ["kachna", "duck"], ["opice", "monkey"],
                ["žirafa", "giraffe"], ["medvěd", "bear"], ["had", "snake"], ["kojot", "coyote"], ["morče", "guinea pig"], ["prase", "pig"], ["křeček", "hamster"]]
    elif idBalicku == -2:
        return [["aréna", "arena"], ["porazit", "beat"], ["kapitán", "captain"], ["okruh", "circuit"], ["trenér", "coach"], ["závodiště", "course"], ["kurt", "court"],
                ["remizovat", "draw"], ["vyřadit", "eliminate"], ["fanoušek", "fan"], ["dostat se do formy", "get fit"], ["zranit se", "get injured"], ["tělocvična", "gym"], ["prohrát", "lose"],
                ["hráč", "player"], ["bazén", "pool"], ["rozhodčí", "referee"], ["vyhrát", "win"], ["cvičit", "work out"], ["yoga", "jóga"], ["svah", "slope"]]
    else:
        return [["lilek", "aubergine"], ["pečený", "baked"], ["fazole", "beans"], ["hovězí", "beef"], ["červená řepa", "beetroot"], ["vařený", "boiled"], ["zelí", "cabbage"],
                ["třešně", "cherries"], ["kuře", "chicken"], ["cuketa", "courgette"], ["krab", "crab"], ["okurka", "cucumber"], ["kachna", "duck"], ["vejce", "egg"],
                ["ryba", "fish"], ["smažený", "fried"], ["ovoce", "fruit"], ["hrozny", "grapes"], ["grilovaný", "grilled"], ["jehně", "lamb"], ["citron", "lemon"]]

def return_hard_random_cards(idBalicku, numOfPairs):
    return random.sample(return_hard_data_cards(idBalicku), numOfPairs)
    
print(return_hard_data_decks())
