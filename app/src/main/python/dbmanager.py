import mysql.connector
import random


class man:
    myDb = mysql.connector.connect(
        host='sql.endora.cz',
        port='3308',
        user='projek1604399138',
        password='XEWUdfw7',
        database='projek1604399138',
        connect_timeout=15
    )
    myCursor = myDb.cursor()

    @staticmethod
    def insert_deck(nazev, autor, jazykSlov, jazykPrekladu, popis, pocetKaret):
        sql = "INSERT INTO Balicek (nazev, autor, jazykSlov, jazykPrekladu, popis, pocetKaret) VALUES (%s, %s, %s, %s, " \
              "%s, %s) "
        val = (nazev, autor, jazykSlov, jazykPrekladu, popis, pocetKaret)
        man.myCursor.execute(sql, val)
        man.myDb.commit()
        return man.myCursor.rowcount == 1

    @staticmethod
    def insert_card(idBalicku, slovo, preklad):
        sql = "INSERT INTO Karta (idBalicku, slovo, preklad) VALUES (%s, %s, %s)"
        val = (idBalicku, slovo, preklad)
        man.myCursor.execute(sql, val)
        man.myDb.commit()
        return man.myCursor.rowcount == 1

    @staticmethod
    def delete_deck(idBalicku):
        sql = "DELETE FROM Balicek WHERE id=%s"
        sql2 = "DELETE FROM Karta WHERE idBalicku=%s"
        val = (idBalicku,)
        man.myCursor.execute(sql, val)
        man.myCursor.execute(sql2, val)
        man.myDb.commit()

    @staticmethod
    def delete_card(idKarty):
        sql = "DELETE FROM Karta WHERE id=%s"
        val = (idKarty,)
        man.myCursor.execute(sql, val)
        man.myDb.commit()
        return man.myCursor.rowcount != 0

    @staticmethod
    def update_deck(idBalicku, nazev, popis, pocetKaret):
        sql = "UPDATE Balicek SET nazev=%s, popis=%s, pocetKaret=%s WHERE id=%s"
        val = (pocetKaret, nazev, popis, idBalicku)
        man.myCursor.execute(sql, val)
        man.myDb.commit()

    @staticmethod
    def update_card(id, slovo, preklad):
        sql = "UPDATE Karta SET slovo=%s, preklad=%s WHERE id=%s"
        val = (slovo, preklad, id)
        man.myCursor.execute(sql, val)
        man.myDb.commit()

    @staticmethod
    def return_array_of_cards(idBalicku):
        sql = "SELECT * FROM Karta WHERE idBalicku=%s"
        val = (idBalicku,)
        man.myCursor.execute(sql, val)
        retlist = []
        row = man.myCursor.fetchone()
        while row is not None:
            # id     slovo
            retlist.append([row[0], row[2]])
            # id    preklad
            retlist.append([row[0], row[3]])
            row = man.myCursor.fetchone()
        return retlist

    @staticmethod
    def return_deck(idBalicku):
        sql = "SELECT * FROM Balicek WHERE id=%s"
        val = (idBalicku,)
        man.myCursor.execute(sql, val)
        return man.myCursor.fetchone()

    @staticmethod
    def return_array_of_decks():
        sql = "SELECT * FROM Balicek"
        man.myCursor.execute(sql)
        retlist = []
        row = man.myCursor.fetchone()
        while row is not None:
            # id    nazev   autor jazSlovo jazPreklad popis pocetKaret
            retlist.append([row[0], row[1], row[2], row[3], row[4], row[5], row[6]])
            row = man.myCursor.fetchone()
        return retlist

    @staticmethod
    def return_random_cards_of_deck(idBalicku, numOfPairs):
        sql = "SELECT * FROM Karta WHERE idBalicku=%s"
        val = (idBalicku,)
        man.myCursor.execute(sql, val)
        cardList = []
        row = man.myCursor.fetchone()
        while row is not None:
            #               slovo , preklad
            cardList.append([row[2], row[3]])
            row = man.myCursor.fetchone()            
        return random.sample(cardList, numOfPairs)

    @staticmethod
    def is_connected():
        return man.myDb.is_connected()

    @staticmethod
    def connect():
        if not man.is_connected():
            man.myDb = mysql.connector.connect(
                host='sql.endora.cz',
                port='3308',
                user='projek1604399138',
                password='XEWUdfw7',
                database='projek1604399138',
                connect_timeout=15
            )
            man.myCursor = man.myDb.cursor()
    
