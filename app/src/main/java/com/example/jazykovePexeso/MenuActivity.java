package com.example.jazykovePexeso;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

public class MenuActivity extends AppCompatActivity {

    LinearLayout menu;

    //game settings
    boolean readForeignWords = true;
    boolean coloredCards = true;
    int gameWidth = 4;
    int gameHeight = 4;
    int playersCnt =1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        menu = findViewById(R.id.playMenu);

    }

    public void showMenu(View view){
        if(menu.getVisibility() == View.INVISIBLE)
        menu.setVisibility(View.VISIBLE);
        menu.startAnimation(AnimationUtils.loadAnimation(this,R.anim.zoomin));
    }

    public void hideMenu(View view){
        if(menu.getVisibility() == View.VISIBLE)
            menu.startAnimation(AnimationUtils.loadAnimation(this,R.anim.zoomout));
        menu.setVisibility(View.INVISIBLE);
    }

    public void btn_multi_click(View view){
        playersCnt = 2;
        startGame();
    }

    public void btn_single_click(View view){
        playersCnt = 1;
        startGame();
    }

    public void startGame(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        gameHeight = (int) prefs.getString("gameSize", "4x4").charAt(2) - 48;
        gameWidth = (int) prefs.getString("gameSize", "4x4").charAt(0) - 48;
        coloredCards = prefs.getBoolean("ColoredCards", false);
        readForeignWords = prefs.getBoolean("ReadForeignWords", false);
        int deckID = prefs.getInt(getString(R.string.deckID), 0);
        int cardsCount = prefs.getInt(getString(R.string.cardsCount) , 20)*2;
        String language = prefs.getString(getString(R.string.foreignLang), "EN");
        String player1 = prefs.getString("player1", "player 1");
        String player2 = prefs.getString("player2", "player 2");


        Intent intent = new Intent(this, GameActivity.class);
        intent.putExtra("width", gameWidth);
        intent.putExtra("height", gameHeight);
        intent.putExtra("playerCount", playersCnt);
        intent.putExtra("coloredCards", coloredCards);
        intent.putExtra("readForeignWords", readForeignWords);
        intent.putExtra(getString(R.string.deckID), deckID);
        intent.putExtra(getString(R.string.cardsCount), cardsCount);
        intent.putExtra(getString(R.string.foreignLang), language);
        intent.putExtra(getString(R.string.player1), player1);
        intent.putExtra(getString(R.string.player2), player2);
        startActivity(intent);
    }

    public void btn_settings_click(View view){
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    public void btn_deckSel_click(View view){
        Intent intent = new Intent(this, DeckSelectActivity.class);
        startActivity(intent);
    }
}