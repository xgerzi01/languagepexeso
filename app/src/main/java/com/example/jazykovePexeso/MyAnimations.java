package com.example.jazykovePexeso;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class MyAnimations {
    Context context;

    public MyAnimations(Context context){
        this.context = context;
    }


    public void cardImgBtnAnimateAndChange(final ImageView imgBtn, final int newResId) {
        Animation.AnimationListener animListener;
        animListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imgBtn.setImageResource(newResId);
            }
        };
        cardImgBtnAnimateAndChange(imgBtn, animListener);
    }

    public void cardImgBtnAnimateAndChange(final ImageView imgBtn, final Bitmap bitmap) {
        Animation.AnimationListener animListener;
        animListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imgBtn.setImageBitmap(bitmap);
            }
        };
        cardImgBtnAnimateAndChange(imgBtn, animListener);
    }

    public void cardImgBtnAnimateAndChange(final ImageView imgBtn, Animation.AnimationListener animListener) {

        Animation rtCard1 = AnimationUtils.loadAnimation(context, R.anim.rotate_card_pt1);
        final Animation rtCard2 = AnimationUtils.loadAnimation(context, R.anim.rotate_card_pt2);

        final AnimationSet animationSet = new AnimationSet(false);
        animationSet.setFillAfter(true);
        animationSet.addAnimation(rtCard1);
        animationSet.addAnimation(rtCard2);
        imgBtn.startAnimation(animationSet);
        rtCard1.setAnimationListener(animListener);
    }
}
