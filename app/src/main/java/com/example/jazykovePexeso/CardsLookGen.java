package com.example.jazykovePexeso;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Typeface;

import androidx.appcompat.app.AlertDialog;

import com.chaquo.python.PyObject;
import com.chaquo.python.Python;
import com.chaquo.python.android.AndroidPlatform;

/**
 * Class made for creating visual of cards,
 * generated values can be accessed after inti by getBitmaps() and getCardStrings
 */
public class CardsLookGen {
    private final String[] cardStrings;
    private final Bitmap[] bitmaps;
    private  final Bitmap[] mipmapBitmaps;

    public Bitmap[] getBitmaps() {
        return bitmaps;
    }

    public String[] getCardStrings() {
        return cardStrings;
    }

    public Bitmap[] getMipmapBitmaps(){return mipmapBitmaps;}

    private final boolean coloredCards;
    private final Context context;

    public CardsLookGen(boolean coloredCards, Context context, int numOfCards, int deckID){
        this.coloredCards = coloredCards;
        this.context = context;
        cardStrings = genCardStrings(deckID, numOfCards);
        bitmaps = genBitmaps(numOfCards);
        mipmapBitmaps =  genMipmaps(bitmaps, numOfCards);
    }


    private Bitmap[] genBitmaps(int numberOfCards) {

        //Blue or colored border of card
        RectF outerRect = new RectF(0, 0, 300, 150);
        Paint rectPaint = new Paint();
        rectPaint.setColor(Color.rgb(0, 120, 180));
        rectPaint.setStyle(Paint.Style.FILL);

        //inner color of card
        Path polygon = new Path();
        polygon.reset();
        polygon.moveTo(20, 10);
        polygon.lineTo(280, 10);
        polygon.lineTo(290, 20);
        polygon.lineTo(290, 130);
        polygon.lineTo(280, 140);
        polygon.lineTo(20,140);
        polygon.lineTo(10, 130);
        polygon.lineTo(10, 20);
        polygon.lineTo(20, 10);
        Paint polyPaint = new Paint();
        polyPaint.setColor(Color.rgb(202, 240, 244));
        polyPaint.setStyle(Paint.Style.FILL);
        polyPaint.setStrokeWidth(5);

        //stroke
        Paint strokePaint = new Paint();
        strokePaint.setStyle(Paint.Style.STROKE);
        strokePaint.setColor(Color.BLACK);
        strokePaint.setAntiAlias(true);

        //text
        Paint paint = new Paint();
        paint.setColor(Color.BLACK); // Text Color
        paint.setTextSize(50); // Text Size
        paint.setStyle(Paint.Style.FILL);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setAntiAlias(true);
        paint.setTextAlign(Paint.Align.CENTER);


        Bitmap[] newBitmaps = new Bitmap[numberOfCards];

        Bitmap help = Bitmap.createBitmap(300, 150, Bitmap.Config.ARGB_8888);
        for (int i = 0; i < numberOfCards; i++) {
            newBitmaps[i] = help.copy(help.getConfig(), true);
            Canvas newCard1 = new Canvas(newBitmaps[i]);
            int textSize;
            if (cardStrings[i].length() > 10)
                textSize = (int)(50 * (11.0 / (cardStrings[i].length() + 1)));
            else
                textSize = 50;
            int posX = newCard1.getWidth() / 2;
            int posY = 75 + textSize / 4;
            if(coloredCards)
                rectPaint.setColor(Color.rgb(colors[i / 2][0], colors[i / 2][1], colors[i / 2][2]));
            newCard1.drawRoundRect(outerRect, 13, 13, rectPaint);
            newCard1.drawPath(polygon, polyPaint);
            paint.setTextSize(textSize); // Text Size
            newCard1.drawText(cardStrings[i], posX, posY, paint);
            newCard1.drawRoundRect(outerRect, 13, 13, strokePaint);
            newCard1.drawPath(polygon, strokePaint);
        }

        return newBitmaps;

    }

    private String[] genCardStrings(int deckID, int numOfCards) {
        int numOfPairs = numOfCards / 2;
        String[] retStrings = new String[numOfCards];
        PyObject retCards;
        if (!Python.isStarted())
            Python.start(new AndroidPlatform(context));
        Python py = Python.getInstance();

        if (deckID > 0) {
            try {
                PyObject databaseMan = py.getModule("dbmanager").callAttr("man");
                retCards = databaseMan.callAttr("return_random_cards_of_deck", deckID, numOfPairs);
            } catch (Exception ex){
                new AlertDialog.Builder(context).setMessage("Cant connect to database:\nYour deck was set to default").show();
                return genCardStrings(0, numOfCards);
            }
        } else{
            PyObject hardData = py.getModule("hard_data");
            retCards = hardData.callAttr("return_hard_random_cards", deckID, numOfPairs);
        }

        for (int i = 0; i < numOfPairs; i++) {
            retStrings[i * 2] = retCards.asList().get(i).asList().get(0).toString();
            retStrings[i * 2 + 1] = retCards.asList().get(i).asList().get(1).toString();
        }
        return retStrings;
    }

    private Bitmap[] genMipmaps(Bitmap[] bitmaps, int numberOfCards){
        Bitmap[] retMaps = new Bitmap[numberOfCards];
        for (int i =0; i < numberOfCards; i++){
            retMaps[i]= Bitmap.createScaledBitmap(bitmaps[i], 150, 75, true);
        }
        return  retMaps;
    }

    private final int [][] colors = {
                {0, 130, 200},  //blue
                {255, 225, 25}, //yellow
                {128, 128, 128},//grey
                {230, 25, 75},  //red
                {245, 130, 48}, //orange
                {220, 190, 255},//lavender
                {128, 0, 0},    //maroon
                {60, 180, 75},  //green
                {0, 0, 128},    //navy
                {70, 240, 240}, //cyan
                {240, 50, 230}, //magenta
                {250, 190, 212},//pink
                {0, 128, 128},  //teal
                {170, 110, 40}, //brown
                {255, 250, 200},//beige
                {170, 255, 195},//mint
                {210, 245, 60}, //lime
                {128, 128, 0},  //olive
                {255, 215, 180},//apricot
                {145, 30, 180}, //purple
    };
}
