package com.example.jazykovePexeso;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chaquo.python.PyObject;

import java.util.ArrayList;
import java.util.List;

public class DynamicViewGen {

    /**
     * Function for dynamical generation of buttons representing game board of pexeso
     *
     * @param ll Horizontal linear layout where the cards buttons should be
     * @param cols Number of columns
     * @param rows Number of rows
     * @param onClickLis OnClickListener for handling on buttons clicks
     * @param dm display metrics
     */
    public static void generateBtns(LinearLayout ll, int cols, int rows, View.OnClickListener onClickLis, @org.jetbrains.annotations.NotNull DisplayMetrics dm) {
        int width = (dm.widthPixels) / cols - 20;
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width, width / 2);
        for (int y = 0; y < rows; y++) {
            LinearLayout newLinearLayout = new LinearLayout(ll.getContext());
            newLinearLayout.setGravity(Gravity.CENTER);
            newLinearLayout.setPadding(5, 5, 5, 5);
            newLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
            ll.addView(newLinearLayout);
            for (int x = 0; x < cols; x++) {
                ImageButton btn = new ImageButton(newLinearLayout.getContext());
                btn.setImageResource(R.drawable.podelna_karta);
                btn.setScaleType(ImageView.ScaleType.FIT_XY);
                btn.setOnClickListener(onClickLis);
                btn.setPadding(0, 0, 0, 0);
                btn.setTag(R.id.posX, x);
                btn.setTag(R.id.posY, y);
                lp.setMargins(5, 5, 5, 5);
                newLinearLayout.addView(btn, lp);
            }
        }
    }


    /**
     *
     * @param ll Vertical linear layout where the info of decks should be
     * @param decks list of PyObjects representing decks from databse
     * @param listener OnClickListener for handling on buttons clicks
     * @param dm display metrics
     * @param prefs shared preferences for essential deck info
     * @return if deck is selected
     */
    public static  ImageButton generateDeckInfo(LinearLayout ll, List<PyObject> decks, View.OnClickListener listener, DisplayMetrics dm, SharedPreferences prefs){
        ImageButton retBtn = null;

        //display info
        int width = dm.widthPixels - 7*(int) (dm.ydpi / 160);
        int height = (int) (100 * dm.scaledDensity );
        Context context = ll.getContext();

        for (PyObject d : decks) {
            List<PyObject> deck = d.asList();
            //layout with info
            LinearLayout deckInfoLay = new LinearLayout(ll.getContext());
            LinearLayout.LayoutParams DIL_param = new LinearLayout.LayoutParams(width + 10, height + 10);
            deckInfoLay.setOrientation(LinearLayout.HORIZONTAL);
            deckInfoLay.setLayoutParams(DIL_param);

            //button
            ImageButton btn = new ImageButton(context);
            LinearLayout.LayoutParams btn_param = new LinearLayout.LayoutParams((width + 10) / 5 - (int) (10 * dm.xdpi / 160), height + 10);
            btn.setLayoutParams(btn_param);
            ll.addView(deckInfoLay, DIL_param);
            btn.setBackgroundResource(R.drawable.custom_btn_deck_info);

            //id of deck
            int deckID = deck.get(0).toInt();
            btn.setTag(R.id.deckID, deckID);
            //number of cards in deck
            int cardCount = deck.get(6).toInt();
            btn.setTag(R.id.numOfCards, cardCount);
            //foreign language of deck
            String language = deck.get(4).toString();
            btn.setTag(R.id.language, language);

            if ((prefs.getInt(context.getString(R.string.deckID), 0)) == deckID) {
                btn.setImageResource(R.drawable.btn_selected);
                retBtn = btn;
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt(context.getString(R.string.deckID), deckID);
                editor.putInt(context.getString(R.string.cardsCount), cardCount);
                editor.putString(context.getString(R.string.foreignLang), language);
                editor.apply();
            } else
                btn.setImageResource(R.drawable.btn_not_selected);
            btn.setScaleType(ImageView.ScaleType.FIT_CENTER);
            btn.setOnClickListener(listener);
            deckInfoLay.addView(btn);

            //textInfo layout
            LinearLayout textInfoLay = new LinearLayout(context);
            LinearLayout.LayoutParams TIL_param = new LinearLayout.LayoutParams((width + 10) * 4 / 5, height + 10);
            textInfoLay.setLayoutParams(TIL_param);
            textInfoLay.setOrientation(LinearLayout.VERTICAL);
            textInfoLay.setBackgroundResource(R.drawable.deck_info_custom_border);
            textInfoLay.setPadding(5, 5, 5, 5);
            deckInfoLay.addView(textInfoLay, 0);
            //name
            {
                TextView tv_name = new TextView(context);
                tv_name.setText(deck.get(1).toString());
                LinearLayout.LayoutParams name_param = new LinearLayout.LayoutParams(width * 4 / 5, height / 4);
                tv_name.setLayoutParams(name_param);
                tv_name.setTextSize(16);
                tv_name.setTypeface(Typeface.DEFAULT_BOLD);
                tv_name.setPadding(5, 0, 5, 0);
                textInfoLay.addView(tv_name);
            }

            //description
            {
                TextView tv_desc = new TextView(context);
                tv_desc.setText(deck.get(5).toString());
                LinearLayout.LayoutParams desc_param = new LinearLayout.LayoutParams(width * 4 / 5, height * 2 / 4);
                tv_desc.setLayoutParams(desc_param);
                tv_desc.setPadding(5, 0, 5, 0);
                tv_desc.setTextSize(12);
                textInfoLay.addView(tv_desc);
            }

            //autor and languages
            {
                LinearLayout AaL_lay = new LinearLayout(context);
                LinearLayout.LayoutParams ALL_param = new LinearLayout.LayoutParams(width * 4 / 5, height / 4);
                AaL_lay.setLayoutParams(ALL_param);
                AaL_lay.setOrientation(LinearLayout.HORIZONTAL);
                textInfoLay.addView(AaL_lay);

                //languages
                {
                    TextView tv_lnag = new TextView(context);
                    tv_lnag.setText(String.format("Z %s do %s", deck.get(3).toString(), deck.get(4).toString()));
                    LinearLayout.LayoutParams lang_param = new LinearLayout.LayoutParams(width * 4 / 10, height / 4);
                    tv_lnag.setLayoutParams(lang_param);
                    tv_lnag.setPadding(5, 0, 5, 0);
                    AaL_lay.addView(tv_lnag);
                }

                //Autor
                {
                    TextView tv_author = new TextView(context);
                    tv_author.setText(deck.get(2).toString());
                    LinearLayout.LayoutParams author_param = new LinearLayout.LayoutParams(width * 4 / 10, height / 4);
                    tv_author.setLayoutParams(author_param);
                    tv_author.setGravity(Gravity.END);
                    tv_author.setPadding(5, 0, 5, 0);
                    AaL_lay.addView(tv_author);
                }
            }
        }
        return retBtn;
    }

    public static  ImageButton generateDeckInfo(LinearLayout ll, List<PyObject> decks, View.OnClickListener listener,
                                                DisplayMetrics dm, SharedPreferences prefs,
                                                String langF, String langTo, String name, String author){
        decks = filter_decks(decks, langF, langTo, name, author);
        return generateDeckInfo(ll, decks, listener, dm, prefs);
    }

    private static List<PyObject> filter_decks(List<PyObject> decks, String langF, String langTo, String name, String author) {
        ArrayList<PyObject> delList = new ArrayList<>();
        for (PyObject d : decks) {
            List<PyObject> deck = d.asList();

            if (name != null)
                if (!deck.get(1).toString().toLowerCase().contains(name.toLowerCase())) {
                    delList.add(d);
                }

            if (author != null)
                if (!deck.get(2).toString().toLowerCase().contains(author.toLowerCase())) {
                    delList.add(d);
                }

            if (langF != null)
                if (!deck.get(3).toString().toLowerCase().equals(langF.toLowerCase())) {
                    delList.add(d);
                }

            if (langTo != null)
                if (!deck.get(4).toString().toLowerCase().equals(langTo.toLowerCase())) {
                    delList.add(d);
                }

        }
        decks.removeAll(delList);
        return decks;
    }
}
