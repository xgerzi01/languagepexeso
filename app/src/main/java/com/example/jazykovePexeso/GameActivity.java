package com.example.jazykovePexeso;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.speech.tts.TextToSpeech;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.chaquo.python.PyObject;
import com.chaquo.python.Python;
import com.chaquo.python.android.AndroidPlatform;

import java.util.List;
import java.util.Locale;

public class GameActivity extends AppCompatActivity {

    ImageView[] imgViews = new ImageView[2];
    TextView tv_moves, tv_score;
    TextView[] players_TVs = new TextView[2];
    Chronometer timer;
    LinearLayout infoLay;
    int moves = 0;



    ImageButton[] rotatedCards = new ImageButton[2];
    int[] lastRotatedCard = new int[2];
    View.OnClickListener onClickLis;
    int cardsFlipped = 0;
    CountDownTimer turnBackTimer;
    boolean paused = false;
    boolean matched = false;
    TextToSpeech ladyGoogle;
    MediaPlayer media;

    PyObject main, player1, player2, pexeso;

    boolean readForeignWords = true;
    boolean coloredCards = true;
    int gameWidth = 4;
    int gameHeight = 4;
    int playersCnt;
    String foreignLang = "EN";

    MyAnimations anim = new MyAnimations(this);
    CardsLookGen cards;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        //getting Views
        LinearLayout ll = findViewById(R.id.linLay_vertical);
        imgViews[0] = findViewById(R.id.firstImgView);
        imgViews[1] = findViewById(R.id.secondImgView);
        timer = findViewById(R.id.timer);
        tv_moves = findViewById(R.id.tv_moves);
        tv_score = findViewById(R.id.tv_score);



        //reading settings from Intent
        Intent intent = getIntent();
        readForeignWords = intent.getBooleanExtra("readForeignWords", false);
        coloredCards = intent.getBooleanExtra("coloredCards", true);
        gameWidth = intent.getIntExtra("width", 4);
        gameHeight = intent.getIntExtra("height", 6);
        playersCnt = intent.getIntExtra("playerCount", 1);
        int deckID = intent.getIntExtra(getString(R.string.deckID), 0);
        int cardsCount = intent.getIntExtra(getString(R.string.cardsCount), 40);
        foreignLang = intent.getStringExtra(getString(R.string.foreignLang));

        //adjusting game board size
        if(cardsCount < gameWidth * gameHeight){
            gameWidth = 4;
            gameHeight = cardsCount /4;
        }


        //timer for turning back cards
        turnBackTimer = new CountDownTimer(1500, 1500) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                if (!matched) {
                    anim.cardImgBtnAnimateAndChange(rotatedCards[0], R.drawable.podelna_karta);
                    anim.cardImgBtnAnimateAndChange(rotatedCards[1], R.drawable.podelna_karta);
                    rotatedCards[0].setClickable(true);
                    rotatedCards[1].setClickable(true);
                } else {
                    setLocked(rotatedCards[0]);
                    setLocked(rotatedCards[1]);
                }
                anim.cardImgBtnAnimateAndChange(imgViews[0], R.drawable.podelna_karta);
                anim.cardImgBtnAnimateAndChange(imgViews[1], R.drawable.podelna_karta);

                paused = false;

            }
        };
        onClickLis = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!paused)
                    onClickBtn(v);
            }
        };

        //text to speech
        if(readForeignWords) {
            ladyGoogle = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status != TextToSpeech.ERROR) {
                        ladyGoogle.setLanguage(getLocaleFromString(foreignLang));
                    }
                }
            });
        }
        media = MediaPlayer.create(this, R.raw.cards_matched);

        //Game board generation
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        DynamicViewGen.generateBtns(ll, gameWidth, gameHeight, onClickLis, dm);
        cards = new CardsLookGen(coloredCards, this, gameWidth * gameHeight, deckID);

        //Python
        if (!Python.isStarted())
            Python.start(new AndroidPlatform(this));
        Python py = Python.getInstance();
        main = py.getModule("pexeso");

        //Single / multi stuff
        player1 = main.callAttr("Player", "milacek");
        players_TVs[0] = findViewById(R.id.tv_P1);
        players_TVs[0].setText(intent.getStringExtra(getString(R.string.player1)));
        if(playersCnt == 1) {
            pexeso = main.callAttr("Pexeso", gameWidth, gameHeight, player1);
            timer.start();
            tv_moves.setText(String.format(getString(R.string.movesFormat), moves));
            infoLay = findViewById(R.id.linLay_single);
        }
        else if (playersCnt == 2){
            player2 = main.callAttr("Player");
            pexeso = main.callAttr("Pexeso", gameWidth, gameHeight, player1, player2);
            infoLay = findViewById(R.id.linLay_multi);
            players_TVs[1] = findViewById(R.id.tv_P2);
            players_TVs[1].setText(intent.getStringExtra(getString(R.string.player2)));
            tv_score.setText(String.format(getString(R.string.scoreFormat), 0, 0));
        }

        infoLay.setVisibility(View.VISIBLE);


    }


    public void onClickBtn(View view) {
        view.setClickable(false);
        int x = (int) view.getTag(R.id.posX);
        int y = (int) view.getTag(R.id.posY);


        int card = (pexeso.callAttr("get_card", x, y)).toInt();
        anim.cardImgBtnAnimateAndChange((ImageButton) view, cards.getMipmapBitmaps()[card]);
        anim.cardImgBtnAnimateAndChange(imgViews[cardsFlipped], cards.getBitmaps()[card]);

        rotatedCards[cardsFlipped] = (ImageButton) view;
        ++cardsFlipped;
        if (cardsFlipped == 2) {
            paused = true;
            matched = pexeso.callAttr("play_move", lastRotatedCard[0], lastRotatedCard[1], x, y).toBoolean();
            turnBackTimer.start();
            cardsFlipped = 0;
            if (matched) {
                media.start();
                view.setClickable(false);
                rotatedCards[0].setClickable(false);
                if (pexeso.callAttr("is_finished").toBoolean()){
                    onFinish();
                }
            }
        }
        lastRotatedCard[0] = x;
        lastRotatedCard[1] = y;

        if (readForeignWords && card % 2 == 1) {
            ladyGoogle.speak(cards.getCardStrings()[card], TextToSpeech.QUEUE_FLUSH, null);
        }


        moves++;
        if(playersCnt == 1) {
            tv_moves.setText(String.format(getString(R.string.movesFormat), (moves + 1) / 2));
        }
        else if (playersCnt == 2){
            List<PyObject> score = pexeso.callAttr("get_score").asList();
            tv_score.setText(String.format(getString(R.string.scoreFormat), score.get(0).toInt(), score.get(1).toInt()));
            int actPlayerIndex = pexeso.callAttr("get_act_player_index").toInt();
            players_TVs[actPlayerIndex].setTextColor(Color.RED);
            players_TVs[(actPlayerIndex +1) % 2].setTextColor(Color.BLACK);

        }
    }



    //adds gray filter to ImageButton, card was taken
    private static void setLocked(ImageView v) {
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(1);
        ColorMatrixColorFilter cf = new ColorMatrixColorFilter(matrix);
        v.setColorFilter(cf);
        v.setImageAlpha(128);
    }

    private Locale getLocaleFromString(String string){
        switch (string){
            case "DE":
                return Locale.GERMANY;
            default:
                readForeignWords = false;
            case "EN":
                return  Locale.ENGLISH;
        }
    }

    @SuppressLint("DefaultLocale")
    private void onFinish(){
        String text = "";
        if(playersCnt == 1){
            timer.stop();
            text = String.format("Gratuluji %s!\nDokončil si hru:\n%s\nPočet tahů: %d", players_TVs[0].getText(), timer.getText(), (moves + 1) >> 1);
        }
        else if (playersCnt == 2){
            List<PyObject> score = pexeso.callAttr("get_score").asList();
            text = String.format("Sláva vítězi, čest poraženým!\nFinální skóre:\n%s\t%d:%d\t%s",players_TVs[0].getText(), score.get(0).toInt(), score.get(1).toInt(), players_TVs[1].getText());
        }
        new AlertDialog.Builder(this).setMessage(text).setTitle("Gratulace!").show();
    }

    public void btnBack_clikc(View v){
        finish();
    }

    public  void btnReset_click(View v){
        recreate();
    }
}