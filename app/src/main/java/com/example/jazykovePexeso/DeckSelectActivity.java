package com.example.jazykovePexeso;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import com.chaquo.python.PyObject;
import com.chaquo.python.Python;
import com.chaquo.python.android.AndroidPlatform;

import java.util.List;

public class DeckSelectActivity extends AppCompatActivity {

    PyObject hard_data;
    LinearLayout ll;
    ImageButton selectedBtn;
    ProgressBar progressBar;
    SharedPreferences prefs;
    boolean error = false;
    Python py;
    List<PyObject> decks;
    View.OnClickListener selectBtn_listener;

    Spinner spin_langFrom;
    Spinner spin_langTo;
    TextView tv_name;
    TextView tv_author;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deck_select);

        ll = findViewById(R.id.linLay_deckInfo);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        progressBar = findViewById(R.id.progressBar2);

        //spinners
        spin_langFrom = findViewById(R.id.spin_langFrom);
        spin_langTo = findViewById(R.id.spin_langTo);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.languages_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_langFrom.setAdapter(adapter);
        spin_langTo.setAdapter(adapter);

        tv_name = findViewById(R.id.txtIn_deckName);
        tv_author = findViewById(R.id.txtIn_Autor);

        selectBtn_listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectBtn_click(v);
            }
        };

        //PYTHON
        if (!Python.isStarted())
            Python.start(new AndroidPlatform(this));
        py = Python.getInstance();

        hard_data = py.getModule("hard_data");

        new databaseLoad().execute(py);
    }

    public void selectBtn_click(View v) {
        ImageButton view = (ImageButton) v;
        if (view != selectedBtn) {
            view.setImageResource(R.drawable.btn_selected);
            if (selectedBtn != null)
                selectedBtn.setImageResource(R.drawable.btn_not_selected);
            selectedBtn = view;

            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt(getString(R.string.deckID), (int) view.getTag(R.id.deckID));
            editor.putInt(getString(R.string.cardsCount), (int) view.getTag(R.id.numOfCards));
            editor.putString(getString(R.string.foreignLang), (String) view.getTag(R.id.language));
            editor.apply();
        }
    }

    public void backBtn_click(View view) {
        finish();
    }


    @SuppressLint("StaticFieldLeak")
    private class databaseLoad extends AsyncTask<Python, Integer, PyObject> {

        @Override
        protected PyObject doInBackground(Python... objects) {
            try {
                PyObject man = (objects[0]).getModule("dbmanager").callAttr("man");
                man.callAttr("connect");
                error = !(man.callAttr("is_connected").toBoolean());
                return man;

            } catch (Exception ex) {
                error = true;
                return null;
            }
        }

        @Override
        protected void onPostExecute(PyObject o) {
            decks = hard_data.callAttr("return_hard_data_decks").asList();

            if (!error) {
                decks.addAll(o.callAttr("return_array_of_decks").asList());
            } else {
                new AlertDialog.Builder(ll.getContext()).setMessage("Cant connect to database").show();
            }
            progressBar.setVisibility(View.GONE);


            String deckName = tv_name.getText().toString();
            String author = tv_author.getText().toString();
            String langFrom = spin_langFrom.getSelectedItem().toString();
            String langTo = spin_langTo.getSelectedItem().toString();

            if(deckName.equals("")) {
                deckName = null;
            }
            if (author.equals("")) {
                author = null;
            }
            if (langFrom.equals("---")) {
                langFrom = null;
            }
            if (langTo.equals("---")) {
                langTo = null;
            }
            //generating info of decks
            DisplayMetrics dm = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(dm);
            ll.removeAllViews();
            selectedBtn = DynamicViewGen.generateDeckInfo(ll, decks, selectBtn_listener, dm, prefs, langFrom, langTo, deckName, author);
        }
    }

    public void search_btn_click(View v) {
        ll.removeAllViews();
        progressBar.setVisibility(View.VISIBLE);
        new databaseLoad().execute(py);


    }
}

